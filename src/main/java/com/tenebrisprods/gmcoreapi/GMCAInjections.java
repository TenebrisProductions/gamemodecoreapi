package com.tenebrisprods.gmcoreapi;

import com.tenebrisprods.gmcoreapi.gamemode.GameMode;
import com.tenebrisprods.gmcoreapi.gamemode.GameModeRegistry;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiCreateWorld;
import net.minecraft.entity.player.PlayerCapabilities;
import net.minecraft.world.WorldSettings.GameType;
import net.minecraft.world.storage.WorldInfo;

/**
 * Created by Dragoniko55 on 2016-08-05.
 */
public class GMCAInjections
{
    public static String recreateFromExistingWorldHook(WorldInfo original, String currentGameMode)
    {
        GameMode gameMode = GameModeRegistry.getGameMode(original.getGameType().getName());

        if (gameMode != null)
        {
            return gameMode.name;
        }

        return currentGameMode;
    }

    public static void showMoreWorldOptionsHook(String gameModeName, GuiButton btnGenStructure, GuiButton btnAllowCheats, GuiButton btnWorldType, GuiButton btnCustomizeType, GuiButton btnBonusChest)
    {
        GameMode gameMode = GameModeRegistry.getGameMode(gameModeName);

        if (gameMode != null)
        {
            btnGenStructure.visible = btnGenStructure.visible && gameMode.worldOptionsVisibility.generateStructures;
            btnAllowCheats.visible = btnAllowCheats.visible && gameMode.worldOptionsVisibility.allowCheats;
            btnWorldType.visible = btnWorldType.visible && gameMode.worldOptionsVisibility.worldType;
            btnCustomizeType.visible = btnCustomizeType.visible && gameMode.worldOptionsVisibility.worldType;
            btnBonusChest.visible = btnBonusChest.visible && gameMode.worldOptionsVisibility.bonusChest;
        }
    }

    public static boolean actionPerformedHook(String gameModeName, GuiButton buttonPressed, GuiCreateWorld guiCreateWorld)
    {
        if (GameModeRegistry.getGameModeSize() > 0 && buttonPressed.enabled && buttonPressed.id == 2)
        {
            int gameModeIndex = -1;
            if (gameModeName.equals("creative") || (gameModeIndex = GameModeRegistry.getGameModeIndex(gameModeName)) != -1)
            {
                gameModeIndex++;
                if (gameModeIndex > GameModeRegistry.getGameModeSize() - 1)
                {
                    guiCreateWorld.gameMode = "creative";
                    return false;
                }

                GameMode gameMode = GameModeRegistry.getGameMode(gameModeIndex);

                guiCreateWorld.gameMode = gameMode.name;
                guiCreateWorld.generateStructuresEnabled = gameMode.worldOptionsDefaults.generateStructures;
                guiCreateWorld.allowCheats = gameMode.worldOptionsDefaults.allowCheats;
                try
                {
                    guiCreateWorld.selectedIndex = gameMode.worldOptionsDefaults.getWorldTypeIndex();
                }
                catch (Exception ex)
                {
                    guiCreateWorld.selectedIndex = 0;
                }
                guiCreateWorld.bonusChestEnabled = gameMode.worldOptionsDefaults.bonusChest;

                return true;
            }
        }

        return false;
    }

    public static boolean isAdventureHook(GameType gameType)
    {
        GameMode gameMode = GameModeRegistry.getGameMode(gameType.getName());
        return gameMode != null && gameMode.isAdventure();
    }

    public static boolean isCreativeHook(GameType gameType)
    {
        GameMode gameMode = GameModeRegistry.getGameMode(gameType.getName());
        return gameMode != null && gameMode.isCreative();
    }

    public static boolean isSurvivalOrAdventureHook(GameType gameType)
    {
        GameMode gameMode = GameModeRegistry.getGameMode(gameType.getName());
        return gameMode != null && gameMode.isSurvivalOrAdventure();
    }

    public static void configurePlayerCapabilitiesHook(GameType gameType, PlayerCapabilities capabilities)
    {
        GameMode gameMode = GameModeRegistry.getGameMode(gameType.getName());

        if (gameMode != null)
        {
            capabilities.allowEdit = gameMode.gameModeCapabilities.allowEdit;
            capabilities.allowFlying = gameMode.gameModeCapabilities.allowFlying;
            capabilities.disableDamage = gameMode.gameModeCapabilities.disableDamage;
            capabilities.isCreativeMode = gameMode.gameModeCapabilities.isCreativeMode;
            capabilities.isFlying = gameMode.gameModeCapabilities.isFlying;
            capabilities.setPlayerWalkSpeed(gameMode.gameModeCapabilities.playerWalkSpeed);
            capabilities.setFlySpeed(gameMode.gameModeCapabilities.playerFlySpeed);
        }
    }
}
