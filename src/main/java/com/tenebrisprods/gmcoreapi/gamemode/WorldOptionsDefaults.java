package com.tenebrisprods.gmcoreapi.gamemode;

import net.minecraft.world.WorldType;

/**
 * Created by Dragoniko55 on 2016-08-28.
 */
public class WorldOptionsDefaults
{
    public boolean generateStructures, allowCheats, bonusChest;
    public WorldType worldType;

    public WorldOptionsDefaults(boolean generateStructures, boolean allowCheats, boolean bonusChest, WorldType worldType)
    {
        this.generateStructures = generateStructures;
        this.allowCheats = allowCheats;
        this.bonusChest = bonusChest;
        this.worldType = worldType;
    }

    public static WorldOptionsDefaults getDefault()
    {
        return new WorldOptionsDefaults(true, false, false, WorldType.DEFAULT);
    }

    public int getWorldTypeIndex() throws Exception
    {
        for (int i = 0; i < WorldType.worldTypes.length; i++)
        {
            if (WorldType.worldTypes[i] == this.worldType)
            {
                return i;
            }
        }

        throw new Exception("The provided world type has not been registered. WorldType: " + this.worldType.toString());
    }
}