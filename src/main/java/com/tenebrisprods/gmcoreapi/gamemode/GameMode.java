package com.tenebrisprods.gmcoreapi.gamemode;

/**
 * Created by Dragoniko55 on 2016-08-05.
 */
public class GameMode
{
    public String name;
    public WorldOptionsDefaults worldOptionsDefaults;
    public WorldOptionsVisibility worldOptionsVisibility;
    public GameModeType gameModeType;
    public GameModeCapabilities gameModeCapabilities;

    public GameMode(String name, WorldOptionsDefaults worldOptionsDefaults, WorldOptionsVisibility worldOptionsVisibility, GameModeType gameModeType, GameModeCapabilities gameModeCapabilities)
    {
        this.name = name;
        this.worldOptionsDefaults = worldOptionsDefaults;
        this.worldOptionsVisibility = worldOptionsVisibility;
        this.gameModeType = gameModeType;
        this.gameModeCapabilities = gameModeCapabilities;
    }

    public GameMode(String name)
    {
        this(name, WorldOptionsDefaults.getDefault(), WorldOptionsVisibility.getDefault(), GameModeType.SURVIVAL_OR_ADVENTURE, GameModeCapabilities.getDefault());
    }

    public boolean isAdventure()
    {
        return this.gameModeType == GameModeType.ADVENTURE;
    }

    public boolean isCreative()
    {
        return this.gameModeType == GameModeType.CREATIVE;
    }

    public boolean isSurvivalOrAdventure()
    {
        return this.gameModeType == GameModeType.SURVIVAL_OR_ADVENTURE;
    }

    public enum GameModeType
    {
        ADVENTURE, CREATIVE, SURVIVAL_OR_ADVENTURE
    }
}