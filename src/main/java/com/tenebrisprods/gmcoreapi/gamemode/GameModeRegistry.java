package com.tenebrisprods.gmcoreapi.gamemode;

import com.tenebrisprods.gmcoreapi.utils.GameModeEnumHelper;
import net.minecraft.world.WorldSettings.GameType;

import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * Created by Dragoniko55 on 2016-08-05.
 */
public class GameModeRegistry
{
    private static final ArrayList<GameMode> _gameModes = new ArrayList<>();

    public static void registerGameMode(GameMode gameMode, String shortName) throws Exception
    {
        if (GameType.parseGameTypeWithDefault(gameMode.name, GameType.NOT_SET) != GameType.NOT_SET)
        {
            throw new Exception("A GameMode with this name is already registered. GameMode name: " + gameMode.name);
        }

        _gameModes.add(gameMode);
        GameModeEnumHelper.addGameType(gameMode.name.toUpperCase(), nextGameTypeIndex(), gameMode.name, shortName);
    }

    public static void registerGameMode(GameMode gameMode) throws Exception
    {
        registerGameMode(gameMode, getShortName(gameMode));
    }

    private static int nextGameTypeIndex()
    {
        int lastID = 0;

        for (GameType gt : GameType.values())
        {
            if (gt.getID() > lastID)
            {
                lastID = gt.getID();
            }
        }

        return ++lastID;
    }

    private static String getShortName(GameMode gameMode) throws Exception
    {
        for (int i = 1; i < gameMode.name.length(); i++)
        {
            String currentShortName = gameMode.name.substring(0, i);

            if (GameType.parseGameTypeWithDefault(currentShortName, GameType.NOT_SET) == GameType.NOT_SET)
            {
                return currentShortName;
            }
        }

        throw new Exception("A GameMode with this shortName is already registered. GameMode name: " + gameMode.name);
    }

    public static int getGameModeSize()
    {
        return _gameModes.size();
    }

    public static GameMode getGameMode(String gameModeName)
    {
        try
        {
            return _gameModes.stream().filter(g -> g.name.equals(gameModeName)).findFirst().get();
        }
        catch (NoSuchElementException ignored)
        {
            return null;
        }
    }

    public static GameMode getGameMode(int gameModeIndex)
    {
        return _gameModes.get(gameModeIndex);
    }

    public static int getGameModeIndex(String gameModeName)
    {
        for (int i = 0; i < _gameModes.size(); i++)
        {
            if (_gameModes.get(i).name.equals(gameModeName))
            {
                return i;
            }
        }

        return -1;
    }
}
