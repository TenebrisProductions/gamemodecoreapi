package com.tenebrisprods.gmcoreapi.gamemode;

/**
 * Created by Dragoniko55 on 2016-08-28.
 */

/**
 * Used to declare what options are visible in the "More World Options" menu.
 */
public class WorldOptionsVisibility
{
    public boolean generateStructures, allowCheats, worldType, bonusChest;

    /**
     * Object containing the values regarding option accessibility.
     * @param generateStructures Is the "Generate Structures" button visible ? (invisible for all default gamemodes)
     * @param allowCheats Is the "Allow Cheats" button visible ? (f.e. invisible in Hardcore; visible for every other default gamemodes)
     * @param worldType Is the "World Type" button visible ? (invisible for all default gamemodes)
     * @param bonusChest Is the "Bonus Chest" button visible ? (f.e. invisible in Hardcore; visible for every other default gamemodes)
     */
    public WorldOptionsVisibility(boolean generateStructures, boolean allowCheats, boolean worldType, boolean bonusChest)
    {
        this.generateStructures = generateStructures;
        this.allowCheats = allowCheats;
        this.worldType = worldType;
        this.bonusChest = bonusChest;
    }

    /**
     * @return Returns an instance of WorldOptionsVisibility with default values.
     */
    public static WorldOptionsVisibility getDefault()
    {
        return new WorldOptionsVisibility(true, true, true, true);
    }

}
