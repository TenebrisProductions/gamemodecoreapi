package com.tenebrisprods.gmcoreapi.gamemode;

import net.minecraft.entity.player.PlayerCapabilities;

/**
 * Created by Dragoniko55 on 2016-08-28.
 */
public class GameModeCapabilities
{
    public final boolean allowEdit, allowFlying, disableDamage, isCreativeMode, isFlying;
    public final float playerWalkSpeed, playerFlySpeed;

    public GameModeCapabilities(boolean allowEdit, boolean allowFlying, boolean disableDamage, boolean isCreativeMode,
                                boolean isFlying, float playerWalkSpeed, float playerFlySpeed)
    {
        this.allowEdit = allowEdit;
        this.allowFlying = allowFlying;
        this.disableDamage = disableDamage;
        this.isCreativeMode = isCreativeMode;
        this.isFlying = isFlying;
        this.playerWalkSpeed = playerWalkSpeed;
        this.playerFlySpeed = playerFlySpeed;
    }

    public static GameModeCapabilities getDefault()
    {
        PlayerCapabilities pc = new PlayerCapabilities();
        return new GameModeCapabilities(true, false, false, false, false, pc.getWalkSpeed(), pc.getFlySpeed());
    }
}
