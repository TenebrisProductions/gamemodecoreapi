package com.tenebrisprods.gmcoreapi.asm;

import com.tenebrisprods.gmcoreapi.GMCAPlugin;
import org.apache.logging.log4j.Level;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static org.objectweb.asm.Opcodes.ALOAD;

/**
 * Created by Dragoniko55 on 2016-08-24.
 */
public class ASMHelper
{
    public static final String VOID = "V",
                            BOOLEAN = "Z",
                               CHAR = "C",
                               BYTE = "B",
                              SHORT = "S",
                            INTEGER = "I",
                               LONG = "L",
                              FLOAT = "F",
                             DOUBLE = "D";

    public static byte[] transformClass(byte[] bytes, Consumer<ClassNode> transform)
    {
        ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
        ClassNode cn = new ClassNode();
        new ClassReader(bytes).accept(cn, 0);

        GMCAPlugin.log(Level.INFO, String.format("Patching class %s", cn.name));
        transform.accept(cn);

        cn.accept(writer);
        return writer.toByteArray();
    }

    public static void patchMethods(ClassNode cn, HashMap<ObfuscationMapping, Consumer<MethodNode>> patches)
    {
        for (MethodNode method : cn.methods)
        {
            patches.entrySet().stream().filter(entry -> entry.getKey().matches(method)).forEach(entry ->
            {
                GMCAPlugin.log(Level.INFO, String.format("Patching method %s %s", method.name, method.desc));
                entry.getValue().accept(method);
            });
        }
    }

    public static void insertBefore(MethodNode method, Predicate<AbstractInsnNode> predicate, InsnList toInsert)
    {
        ArrayList<Predicate<AbstractInsnNode>> predicates = new ArrayList<>();
        predicates.add(predicate);

        insertBefore(method, predicates, toInsert);
    }

    public static void insertBefore(MethodNode method, List<Predicate<AbstractInsnNode>> targetPredicates, InsnList toInsert)
    {
        AbstractInsnNode[] instructions = method.instructions.toArray();
        AbstractInsnNode target = null;

        for (int i = 0; i < instructions.length; i++)
        {
            AbstractInsnNode first = instructions[i];

            int predicateIndex = 0;
            for (int j = i; j < instructions.length; j++)
            {
                AbstractInsnNode node = instructions[j];

                if (predicateIndex >= targetPredicates.size())
                {
                    break;
                }

                if (!targetPredicates.get(predicateIndex++).test(node))
                {
                    first = null;
                    break;
                }
            }

            if (first != null)
            {
                target = first;
                break;
            }
        }

        if (target != null)
        {
            method.instructions.insertBefore(target, toInsert);
        }
    }

    public static void addInsnUsingThisObj(InsnList list, AbstractInsnNode insn)
    {
        list.add(new VarInsnNode(ALOAD, 0));
        list.add(insn);
    }

    public static String getTypeSignature(String type)
    {
        return getSignature(null, type);
    }

    public static String getSignature(String returnType, String... parameterTypes)
    {
        StringBuilder sb = new StringBuilder();

        for (String type : parameterTypes)
        {
            sb.append(getType(type));
        }

        return returnType != null ? String.format("(%s)%s", sb.toString(), getType(returnType)) : sb.toString();
    }

    private static String getType(String name)
    {
        if (name.equals(VOID) || name.equals(BOOLEAN) || name.equals(CHAR) || name.equals(BYTE) || name.equals(SHORT) ||
                name.equals(INTEGER) || name.equals(LONG) || name.equals(FLOAT) || name.equals(DOUBLE))
        {
            return name;
        }

        return String.format("L%s;", name);
    }
}
