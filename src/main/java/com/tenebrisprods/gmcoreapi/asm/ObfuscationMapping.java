package com.tenebrisprods.gmcoreapi.asm;

import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.common.io.LineProcessor;
import com.google.common.io.Resources;
import net.minecraft.launchwrapper.Launch;
import net.minecraftforge.common.ForgeVersion;
import net.minecraftforge.fml.common.asm.transformers.deobf.FMLDeobfuscatingRemapper;
import net.minecraftforge.fml.relauncher.FMLInjectionData;
import org.objectweb.asm.commons.Remapper;
import org.objectweb.asm.tree.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.tenebrisprods.gmcoreapi.utils.Utils.getFieldAsType;
import static org.objectweb.asm.Opcodes.*;

/**
 * Created by Dragoniko55 on 2016-08-20.
 */
public class ObfuscationMapping
{
    private static ObfuscationRemapper obfuscationRemapper = new ObfuscationRemapper();
    private static Remapper mcpRemapper = null;
    private static final boolean OBFUSCATED;

    private String _owner, _name, _description;

    static
    {
        OBFUSCATED = initialize();
    }

    public ObfuscationMapping(String owner, String name, String description)
    {
        this._owner = owner;
        this._name = name;
        this._description = description;

        if (this._owner.contains("."))
        {
            throw new IllegalArgumentException(String.format("The owner's name cannot contain any \".\" characters! Owner's name: %s", this._owner));
        }
    }

    public ObfuscationMapping(String owner)
    {
        this(owner, "", "");
    }

    public boolean matches(MethodNode method)
    {
        return method.name.equals(this._name) && method.desc.equals(this._description);
    }

    public boolean sameClass(String className)
    {
        return className.replace(".", "/").equals(this._owner);
    }

    public AbstractInsnNode toInsn(int Opcode)
    {
        if (isClass())
        {
            return new TypeInsnNode(Opcode, this._owner);
        }
        else if (isMethod())
        {
            return new MethodInsnNode(Opcode, this._owner, this._name, this._description, Opcode == INVOKEINTERFACE);
        }
        else
        {
            return new FieldInsnNode(Opcode, this._owner, this._name, this._description);
        }
    }

    public ObfuscationMapping toClassloading()
    {
        if (!OBFUSCATED)
        {
            map(mcpRemapper);
        }
        else if (obfuscationRemapper.isObf(this._owner))
        {
            map(obfuscationRemapper);
        }

        return this;
    }

    private ObfuscationMapping map(Remapper mapper)
    {
        // If there is no remapper, then don't map anything
        if (mapper == null)
        {
            return this;
        }

        // Map the name of the instance
        if (isMethod())
        {
            this._name = mapper.mapMethodName(this._owner, this._name, this._description);
        }
        else if (isField())
        {
            this._name = mapper.mapFieldName(this._owner, this._name, this._description);
        }

        // Now that we don't need the original owner anymore, map it
        this._owner = mapper.mapType(this._owner);

        // Map the description of the instance
        if (isMethod())
        {
            this._description = mapper.mapMethodDesc(this._description);
        }
        else if (this._description.length() > 0)
        {
            this._description = mapper.mapDesc(this._description);
        }

        // Return the instance
        return this;
    }

    private boolean isClass()
    {
        return this._name.length() == 0;
    }

    private boolean isMethod()
    {
        return this._description.contains("(");
    }

    private boolean isField()
    {
        return !this.isClass() && !this.isMethod();
    }

    private static boolean initialize()
    {
        boolean obfuscated = true;

        try
        {
            // Find and get the bytes of a small class (less bytes = better performance) using the
            // unobfuscated name. If it is null, it hasn't been found and we are therefore in an
            // obfuscated environment.
            obfuscated = Launch.classLoader.getClassBytes("net.minecraft.world.IBlockAccess") == null;
        }
        catch (IOException ignored) {}

        // If this is an unobfuscated environment, instantiate the mcpRemapper
        if (!obfuscated)
        {
            mcpRemapper = new MCPRemapper();
        }

        return obfuscated;
    }

    private static class ObfuscationRemapper extends Remapper
    {
        private HashMap<String, String> fields = new HashMap<>();
        private HashMap<String, String> methods = new HashMap<>();


        private ObfuscationRemapper()
        {
            try
            {
                // Load mappings from forge
                Map<String, Map<String, String>> test = new HashMap<>();
                Map<String, Map<String, String>> rawFieldMaps = getFieldAsType(FMLDeobfuscatingRemapper.class, "rawFieldMaps", FMLDeobfuscatingRemapper.INSTANCE, test.getClass());
                Map<String, Map<String, String>> rawMethodMaps = getFieldAsType(FMLDeobfuscatingRemapper.class, "rawMethodMaps", FMLDeobfuscatingRemapper.INSTANCE, test.getClass());

                // If there are no mappings, something is not right
                if (rawFieldMaps == null)
                {
                    throw new IllegalStateException(String.format("%s loaded too early. Make sure all references are in or after the asm transformer load stage", ObfuscationMapping.class.getName()));
                }

                // Fill the fields map
                for (Map<String, String> map : rawFieldMaps.values())
                {
                    map.entrySet().stream().filter(entry -> entry.getValue().startsWith("field")).forEach(
                            entry -> fields.put(entry.getValue(), entry.getKey().substring(0, entry.getKey().indexOf(':')))
                    );
                }

                // Fill the methods map
                for (Map<String, String> map : rawMethodMaps.values())
                {
                    map.entrySet().stream().filter(entry -> entry.getValue().startsWith("func")).forEach(
                            entry -> methods.put(entry.getValue(), entry.getKey().substring(0, entry.getKey().indexOf('(')))
                    );
                }
            }
            catch (Exception ex)
            {
                throw new RuntimeException(ex);
            }
        }

        @Override
        public String mapFieldName(String owner, String name, String desc)
        {
            // Return the mapped name for the given field name or said name if no mapping is found
            String s = fields.get(name);
            return s == null ? name : s;
        }

        @Override
        public String mapMethodName(String owner, String name, String desc)
        {
            // Return the mapped name for the given method name or said name if no mapping is found
            String s = methods.get(name);
            return s == null ? name : s;
        }

        @Override
        public String map(String typeName)
        {
            // Return the reverted (unmapped) value from forge
            return FMLDeobfuscatingRemapper.INSTANCE.unmap(typeName);
        }

        private String unmap(String typeName)
        {
            return FMLDeobfuscatingRemapper.INSTANCE.map(typeName);
        }

        private boolean isObf(String typeName)
        {
            return !map(typeName).equals(typeName) || !unmap(typeName).equals(typeName);
        }
    }

    private static class MCPRemapper extends Remapper implements LineProcessor<Void>
    {
        private HashMap<String, String> fields = new HashMap<>();
        private HashMap<String, String> methods = new HashMap<>();

        private MCPRemapper()
        {
            // Get the files for the mappings
            File[] mappings = getConfFiles();

            try
            {
                // Fill both lists
                Resources.readLines(mappings[1].toURI().toURL(), Charsets.UTF_8, this);
                Resources.readLines(mappings[2].toURI().toURL(), Charsets.UTF_8, this);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public String mapMethodName(String owner, String name, String desc)
        {
            // Return the mapped name for the given method name or said name if no mapping is found
            String s = methods.get(name);
            return s == null ? name : s;
        }

        @Override
        public String mapFieldName(String owner, String name, String desc)
        {
            // Return the mapped name for the given field name or said name if no mapping is found
            String s = fields.get(name);
            return s == null ? name : s;
        }

        @Override
        public boolean processLine(String line)
        {
            // Get everything before the comma
            int comma = line.indexOf(',');
            String srg = line.substring(0, comma);

            // Get everything between the first comma and the next one
            String mcp = line.substring(++comma, line.indexOf(',', comma));

            // Add the mapping to the corresponding list
            (srg.startsWith("func") ? methods : fields).put(srg, mcp);

            // Always return true to keep processing until the end
            return true;
        }

        @Override
        public Void getResult()
        {
            return null;
        }

        private static File[] getConfFiles()
        {
            // Try and load the mappings using the directories stored in the GradleStart
            if (!Strings.isNullOrEmpty(System.getProperty("net.minecraftforge.gradle.GradleStart.srgDir")))
            {
                File srgDir = new File(System.getProperty("net.minecraftforge.gradle.GradleStart.srgDir"));
                File csvDir = new File(System.getProperty("net.minecraftforge.gradle.GradleStart.csvDir"));

                if (srgDir.exists() && csvDir.exists())
                {
                    File srg = new File(srgDir, "notch-srg.srg");
                    File fieldCsv = new File(csvDir, "fields.csv");
                    File methodCsv = new File(csvDir, "methods.csv");

                    if (srg.exists() && fieldCsv.exists() && methodCsv.exists())
                    {
                        return new File[]{srg, fieldCsv, methodCsv};
                    }
                }
            }

            // If that did not work, try and load them from common locations
            File mcDir = (File) FMLInjectionData.data()[6];
            File dir = new File(mcDir, "../conf");

            if (!dir.exists() || dir.isFile())
            {
                dir = new File(mcDir, "../build/unpacked/conf");

                if (!dir.exists() || dir.isFile())
                {
                    dir = new File(String.format(
                            "%s.gradle/caches/minecraft/net/minecraftforge/forge/%s-%s/unpacked/conf",
                            System.getProperty("user.home"),
                            FMLInjectionData.data()[4],
                            ForgeVersion.getVersion()));
                }
            }

            if (dir.exists() && !dir.isFile())
            {
                File srgDir = new File(dir, "conf");
                if (!srgDir.exists())
                {
                    srgDir = dir;
                }

                File srgs = new File(srgDir, "packaged.srg");
                if (!srgs.exists())
                {
                    srgs = new File(srgDir, "joined.srg");

                    if (!srgs.exists())
                    {
                        throw new RuntimeException("Could not find either packaged.srg or joined.srg");
                    }
                }

                File mapDir = new File(dir, "mappings");
                if (!mapDir.exists())
                {
                    mapDir = dir;
                }

                File methods = new File(mapDir, "methods.csv");
                if (!methods.exists())
                {
                    throw new RuntimeException("Could not find methods.csv");
                }

                File fields = new File(mapDir, "fields.csv");
                if (!fields.exists())
                {
                    throw new RuntimeException("Could not find fields.csv");
                }

                return new File[] { srgs, methods, fields };
            }

            throw new RuntimeException("Unable to find the directory for the obfuscation mappings.");
        }
    }
}
