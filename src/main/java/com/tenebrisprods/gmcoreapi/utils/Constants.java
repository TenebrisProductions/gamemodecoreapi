package com.tenebrisprods.gmcoreapi.utils;

/**
 * Created by Dragoniko55 on 2016-08-05.
 */
public class Constants 
{
    // Classes
    public static final String STRING = "java/lang/String",
                           GUI_BUTTON = "net/minecraft/client/gui/GuiButton",
                           WORLD_INFO = "net/minecraft/world/storage/WorldInfo",
                  PLAYER_CAPABILITIES = "net/minecraft/entity/player/PlayerCapabilities",
                     GUI_CREATE_WORLD = "net/minecraft/client/gui/GuiCreateWorld",
                            GAME_TYPE = "net/minecraft/world/WorldSettings$GameType",
                      GMCA_INJECTIONS = "com/tenebrisprods/gmcoreapi/GMCAInjections",

    // GMCAInjections methods
                      IsAdventureHook = "isAdventureHook",
                       IsCreativeHook = "isCreativeHook",
            IsSurvivalOrAdventureHook = "isSurvivalOrAdventureHook",
                    ConfPlayerCapHook = "configurePlayerCapabilitiesHook",
             ShowMoreWorldOptionsHook = "showMoreWorldOptionsHook",
        RecreateFromExistingWorldHook = "recreateFromExistingWorldHook",
                  ActionPerformedHook = "actionPerformedHook",

    // GameType methods
                           IsCreative = "func_77145_d",
                IsSurvivalOrAdventure = "func_77144_e",
                          IsAdventure = "func_82752_c",
                        ConfPlayerCap = "func_77147_a",

    // GuiCreateWorld fields
                        GameModeField = "field_146342_r",
                       BtnMapFeatures = "field_146325_B",
                     BtnAllowCommands = "field_146321_E",
                           BtnMapType = "field_146320_D",
                     BtnCustomizeType = "field_146322_F",
                        BtnBonusItems = "field_146326_C",

    // GuiCreateWorld methods
                      ActionPerformed = "func_146284_a",
                   UpdateDisplayState = "func_146319_h",
                 ShowMoreWorldOptions = "func_146316_a",
            RecreateFromExistingWorld = "func_146318_a",

    // Mod related constants
                             MOD_INFO = "/gmcamod.info",
                               MOD_ID = "GMCoreAPI";
}