package com.tenebrisprods.gmcoreapi.utils;

import net.minecraft.world.WorldSettings.GameType;
import net.minecraftforge.common.util.EnumHelper;

/**
 * Created by Dragoniko55 on 2016-08-05.
 */
public class GameModeEnumHelper extends EnumHelper
{
    private static Class<?>[][] gameModeTypes =
    {
        {GameType.class, int.class, String.class, String.class}
    };

    public static GameType addGameType(String name, int id, String displayName, String shortName)
    {
        return addEnum(GameType.class, name, id, displayName, shortName);
    }

    public static <T extends Enum<?>> T addEnum(Class<T> enumType, String enumName, Object... paramValues)
    {
        return addEnum(gameModeTypes, enumType, enumName, paramValues);
    }
}
