package com.tenebrisprods.gmcoreapi.utils;

import java.lang.reflect.Field;

/**
 * Created by Dragoniko55 on 2016-09-01.
 */
public class Utils
{
    public static <T> T as(Class<T> type, Object object)
    {
        return type.isInstance(object) ? type.cast(object) : null;
    }

    public static <T, U> T getFieldAsType(Class<U> aClass, String targetField, U getFrom, Class<T> type) throws NoSuchFieldException, IllegalAccessException
    {
        Field field = aClass.getDeclaredField(targetField);
        field.setAccessible(true);
        T value = as(type, field.get(getFrom));
        field.setAccessible(false);
        return value;
    }
}
