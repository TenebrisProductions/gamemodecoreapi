package com.tenebrisprods.gmcoreapi;

import net.minecraftforge.fml.common.DummyModContainer;
import net.minecraftforge.fml.common.MetadataCollection;

import static com.tenebrisprods.gmcoreapi.utils.Constants.MOD_ID;
import static com.tenebrisprods.gmcoreapi.utils.Constants.MOD_INFO;

/**
 * Created by Dragoniko55 on 2016-09-01.
 */
public class GMCAModContainer extends DummyModContainer
{
    public GMCAModContainer()
    {
        super(MetadataCollection.from(MetadataCollection.class.getResourceAsStream(MOD_INFO), MOD_ID).getMetadataForId(MOD_ID, null));
    }
}
