package com.tenebrisprods.gmcoreapi;

import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

import static com.tenebrisprods.gmcoreapi.utils.Constants.MOD_ID;

/**
 * Created by Dragoniko55 on 2016-08-05.
 */
public class GMCAPlugin implements IFMLLoadingPlugin
{
    private static final Logger logger = LogManager.getLogger(MOD_ID);

    public static void log(Level level, String message)
    {
        logger.log(level, message);
    }

    @Override
    public String[] getASMTransformerClass()
    {
        return new String[] { GMCAClassTransformer.class.getName() };
    }

    @Override
    public String getModContainerClass()
    {
        return GMCAModContainer.class.getName();
    }

    @Override
    public String getSetupClass()
    {
        return null;
    }

    @Override
    public void injectData(Map<String, Object> data)
    {
    }

    @Override
    public String getAccessTransformerClass()
    {
        return null;
    }
}
