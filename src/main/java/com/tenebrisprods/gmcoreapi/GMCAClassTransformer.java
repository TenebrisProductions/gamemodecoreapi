package com.tenebrisprods.gmcoreapi;

import com.tenebrisprods.gmcoreapi.asm.ObfuscationMapping;
import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.tree.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static com.tenebrisprods.gmcoreapi.asm.ASMHelper.*;
import static com.tenebrisprods.gmcoreapi.utils.Constants.*;
import static org.objectweb.asm.Opcodes.*;

/**
 * Created by Dragoniko55 on 2016-08-05.
 */
public class GMCAClassTransformer implements IClassTransformer
{
    // Create the minecraft mappings
    private static ObfuscationMapping gameType = new ObfuscationMapping(GAME_TYPE).toClassloading(),
                                     guiCreate = new ObfuscationMapping(GUI_CREATE_WORLD).toClassloading(),
                               actionPerformed = new ObfuscationMapping(GUI_CREATE_WORLD, ActionPerformed, getSignature(VOID, GUI_BUTTON)).toClassloading(),
                          showMoreWorldOptions = new ObfuscationMapping(GUI_CREATE_WORLD, ShowMoreWorldOptions, getSignature(VOID, BOOLEAN)).toClassloading(),
                     recreateFromExistingWorld = new ObfuscationMapping(GUI_CREATE_WORLD, RecreateFromExistingWorld, getSignature(VOID, WORLD_INFO)).toClassloading(),
                                      gameMode = new ObfuscationMapping(GUI_CREATE_WORLD, GameModeField, getTypeSignature(STRING)).toClassloading(),
                                    isCreative = new ObfuscationMapping(GAME_TYPE, IsCreative, getSignature(BOOLEAN)).toClassloading(),
                         isSurvivalOrAdventure = new ObfuscationMapping(GAME_TYPE, IsSurvivalOrAdventure, getSignature(BOOLEAN)).toClassloading(),
                                   isAdventure = new ObfuscationMapping(GAME_TYPE, IsAdventure, getSignature(BOOLEAN)).toClassloading(),
                                 confPlayerCap = new ObfuscationMapping(GAME_TYPE, ConfPlayerCap, getSignature(VOID, PLAYER_CAPABILITIES)).toClassloading(),
                            updateDisplayState = new ObfuscationMapping(GUI_CREATE_WORLD, UpdateDisplayState, getSignature(VOID)).toClassloading(),
                                btnMapFeatures = new ObfuscationMapping(GUI_CREATE_WORLD, BtnMapFeatures, getTypeSignature(GUI_BUTTON)).toClassloading(),
                              btnAllowCommands = new ObfuscationMapping(GUI_CREATE_WORLD, BtnAllowCommands, getTypeSignature(GUI_BUTTON)).toClassloading(),
                                    btnMapType = new ObfuscationMapping(GUI_CREATE_WORLD, BtnMapType, getTypeSignature(GUI_BUTTON)).toClassloading(),
                              btnCustomizeType = new ObfuscationMapping(GUI_CREATE_WORLD, BtnCustomizeType, getTypeSignature(GUI_BUTTON)).toClassloading(),
                                 btnBonusItems = new ObfuscationMapping(GUI_CREATE_WORLD, BtnBonusItems, getTypeSignature(GUI_BUTTON)).toClassloading(),

    // Create mappings for my methods (no need to translate, only need the utility methods)
                           actionPerformedHook = new ObfuscationMapping(GMCA_INJECTIONS, ActionPerformedHook, getSignature(BOOLEAN, STRING, GUI_BUTTON, GUI_CREATE_WORLD)),
                      showMoreWorldOptionsHook = new ObfuscationMapping(GMCA_INJECTIONS, ShowMoreWorldOptionsHook, getSignature(VOID, STRING, GUI_BUTTON, GUI_BUTTON, GUI_BUTTON, GUI_BUTTON, GUI_BUTTON)),
                 recreateFromExistingWorldHook = new ObfuscationMapping(GMCA_INJECTIONS, RecreateFromExistingWorldHook, getSignature(STRING, WORLD_INFO, STRING)),
                             confPlayerCapHook = new ObfuscationMapping(GMCA_INJECTIONS, ConfPlayerCapHook, getSignature(VOID, GAME_TYPE, PLAYER_CAPABILITIES));

    @Override
    public byte[] transform(String name, String transformedName, byte[] basicClass)
    {
        if (gameType.sameClass(name))
        {
            return transformGameType(basicClass);
        }
        else if (guiCreate.sameClass(name))
        {
            return transformGuiCreate(basicClass);
        }

        return basicClass;
    }

    private static byte[] transformGameType(byte[] bytes)
    {
        return transformClass(bytes, cn ->
        {
            HashMap<ObfuscationMapping, Consumer<MethodNode>> patches = new HashMap<>();

            patches.put(isCreative, GMCAClassTransformer::patchIsCreative);
            patches.put(isSurvivalOrAdventure, GMCAClassTransformer::patchIsSurvivalOrAdventure);
            patches.put(isAdventure, GMCAClassTransformer::patchIsAdventure);
            patches.put(confPlayerCap, GMCAClassTransformer::patchConfPlayerCap);
            patchMethods(cn, patches);
        });
    }

    private static byte[] transformGuiCreate(byte[] bytes)
    {
        return transformClass(bytes, cn ->
        {
            HashMap<ObfuscationMapping, Consumer<MethodNode>> patches = new HashMap<>();

            patches.put(actionPerformed, GMCAClassTransformer::patchActionPerformed);
            patches.put(showMoreWorldOptions, GMCAClassTransformer::patchShowMoreWorldOptions);
            patches.put(recreateFromExistingWorld, GMCAClassTransformer::patchRecreateFromExistingWorld);

            patchMethods(cn, patches);
        });
    }

    private static void patchActionPerformed(MethodNode method)
    {
        InsnList toInsert = new InsnList();
        LabelNode l1 = new LabelNode();

        addInsnUsingThisObj(toInsert, gameMode.toInsn(GETFIELD));
        toInsert.add(new VarInsnNode(ALOAD, 1));
        toInsert.add(new VarInsnNode(ALOAD, 0));
        toInsert.add(actionPerformedHook.toInsn(INVOKESTATIC));
        addInsnUsingThisObj(toInsert, updateDisplayState.toInsn(INVOKESPECIAL));
        toInsert.add(new JumpInsnNode(IFEQ, l1));
        toInsert.add(new InsnNode(RETURN));
        toInsert.add(l1);

        insertBefore(method, n -> n.getOpcode() != -1, toInsert);
    }

    private static void patchShowMoreWorldOptions(MethodNode method)
    {
        ArrayList<Predicate<AbstractInsnNode>> conditions = new ArrayList<>();
        InsnList toInsert = new InsnList();

        conditions.add(n -> n.getOpcode() == ALOAD);
        conditions.add(n -> n.getOpcode() == INVOKESPECIAL);

        addInsnUsingThisObj(toInsert, gameMode.toInsn(GETFIELD));
        addInsnUsingThisObj(toInsert, btnMapFeatures.toInsn(GETFIELD));
        addInsnUsingThisObj(toInsert, btnAllowCommands.toInsn(GETFIELD));
        addInsnUsingThisObj(toInsert, btnMapType.toInsn(GETFIELD));
        addInsnUsingThisObj(toInsert, btnCustomizeType.toInsn(GETFIELD));
        addInsnUsingThisObj(toInsert, btnBonusItems.toInsn(GETFIELD));
        toInsert.add(showMoreWorldOptionsHook.toInsn(INVOKESTATIC));

        insertBefore(method, conditions, toInsert);
    }

    private static void patchRecreateFromExistingWorld(MethodNode method)
    {
        InsnList toInsert = new InsnList();

        toInsert.add(new VarInsnNode(ALOAD, 0));
        toInsert.add(new VarInsnNode(ALOAD, 1));
        addInsnUsingThisObj(toInsert, gameMode.toInsn(GETFIELD));
        toInsert.add(recreateFromExistingWorldHook.toInsn(INVOKESTATIC));
        toInsert.add(gameMode.toInsn(PUTFIELD));

        insertBefore(method, n -> n.getOpcode() == RETURN, toInsert);
    }

    private static void patchConfPlayerCap(MethodNode method)
    {
        InsnList toInsert = new InsnList();

        toInsert.add(new VarInsnNode(ALOAD, 0));
        toInsert.add(new VarInsnNode(ALOAD, 1));
        toInsert.add(confPlayerCapHook.toInsn(INVOKESTATIC));

        insertBefore(method, n -> n.getOpcode() == RETURN, toInsert);
    }

    private static void patchIsCreative(MethodNode method)
    {
        replaceIsMethods(method, new String[] { "CREATIVE" }, IsCreativeHook);
    }

    private static void patchIsSurvivalOrAdventure(MethodNode method)
    {
        replaceIsMethods(method, new String[] { "SURVIVAL", "ADVENTURE" }, IsSurvivalOrAdventureHook);
    }

    private static void patchIsAdventure(MethodNode method)
    {
        replaceIsMethods(method, new String[] { "ADVENTURE", "SPECTATOR" }, IsAdventureHook);
    }

    private static void replaceIsMethods(MethodNode method, String[] staticGets, String staticMethodName)
    {
        ObfuscationMapping staticMethod = new ObfuscationMapping(GMCA_INJECTIONS, staticMethodName, getSignature(BOOLEAN, GAME_TYPE));

        method.instructions = new InsnList();
        LabelNode l1 = new LabelNode();

        for (int i = 0; i < staticGets.length; i++)
        {
            ObfuscationMapping staticGet = new ObfuscationMapping(GAME_TYPE, staticGets[i], getTypeSignature(GAME_TYPE)).toClassloading();

            addInsnUsingThisObj(method.instructions, staticGet.toInsn(GETSTATIC));
            method.instructions.add(new JumpInsnNode(IF_ACMPEQ, l1));
        }

        method.instructions.add(new VarInsnNode(ALOAD, 0));
        method.instructions.add(staticMethod.toInsn(INVOKESTATIC));
        method.instructions.add(new JumpInsnNode(IFNE, l1));
        method.instructions.add(new InsnNode(ICONST_0));
        method.instructions.add(new InsnNode(IRETURN));
        method.instructions.add(l1);
        method.instructions.add(new InsnNode(ICONST_1));
        method.instructions.add(new InsnNode(IRETURN));
    }
}